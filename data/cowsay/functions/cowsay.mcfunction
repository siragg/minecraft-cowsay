
# create armorstand at player position
execute as @p at @s run summon armor_stand ~ ~ ~ 
# give armor_stand the same positon and direction as player
execute as @p at @s run tp @e[type=armor_stand,limit=1] ~ ~ ~ ~ ~

# send the player up int the sky
execute as @p at @s run tp @p[limit=1] ~ 257 ~ 

# summon cow
execute at @p run summon cow ^ ^ ^2 {NoAI:1b,CustomName:"\"Hello there\""}

# turn cows head towards player
execute as @e[type=cow] at @s run teleport @s ~ ~1 ~ facing entity @p

